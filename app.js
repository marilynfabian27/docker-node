
const express = require('express');
const app = express();

app.get('/', (req, res) => {
    res.send('hola mundo');
});

const port = 3000;

var server = app.listen(port, () => {
    console.log(`App is running http://localhost:${port}`);
});

app.server = server;

module.exports = app
