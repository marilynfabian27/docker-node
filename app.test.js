sp = require ("supertest")
app = require ("./app.js")
describe('GET /', ()=>{
	it ("Verificando hola mundo", async ()=>{
		response = await sp(app).get('/')
		expect(response.statusCode).toBe(200)
		expect(response.text).toEqual("hola mundo")
	})

})

beforeAll(done => {
	app.server.close();
	done();
})
